import json
import requests
from static import *


url = "https://arl2.api.myob.com/accountright/53f60d69-ae83-4722-99a8-bdfc30d65040/Contact/Customer"

response = requests.request("GET", url, headers=headers, data=payload)

a = response.json()

arr = []
for i in range(0, len(a['Items'])):
    e = {}
    e['UID'] = a['Items'][i]["UID"]

    if a['Items'][i]["IsIndividual"] == True:
        e['FirstName'] = a['Items'][i]["FirstName"]
        e.update({"Last_Name": a['Items'][i]["LastName"]})
        e['LastName'] = a['Items'][i]["LastName"]

        e.update({"Company_Name": "--"})
    else:
        e.update({"Company_Name": a['Items'][i]["CompanyName"]})
        e.update({"First_Name": "--"})
        e.update({"Last_Name": "--"})

    if "Addresses" in a["Items"][i]:
        if not a['Items'][i]["Addresses"]:
            e['Addresses'] = "--"
        else:
            e['Addresses'] = a['Items'][i]["Addresses"]

    arr.append(e)

path = input("Enter a path : ")
jsonString = json.dumps(arr)
jsonFile = open("{}/customer.json".format(path), "w")
jsonFile.write(jsonString)
jsonFile.close()
